export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBamwICU7COgA6zEq6tQ3a6xSoIhrkybHg",
    authDomain: "atos-trello.firebaseapp.com",
    databaseURL: "https://atos-trello.firebaseio.com",
    projectId: "atos-trello",
    storageBucket: "atos-trello.appspot.com",
    messagingSenderId: "580038306844",
    appId: "1:580038306844:web:83736542f901ad2b5c43d9",
    measurementId: "G-SHWDH78K6G"
  },
  url: "http://localhost:4200"
};
