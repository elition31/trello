// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBamwICU7COgA6zEq6tQ3a6xSoIhrkybHg",
    authDomain: "atos-trello.firebaseapp.com",
    databaseURL: "https://atos-trello.firebaseio.com",
    projectId: "atos-trello",
    storageBucket: "atos-trello.appspot.com",
    messagingSenderId: "580038306844",
    appId: "1:580038306844:web:83736542f901ad2b5c43d9",
    measurementId: "G-SHWDH78K6G"
  },
  url: "http://localhost:4200"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
