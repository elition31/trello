// Angular
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Components
import { GuestLayoutComponent } from "./layouts/guest-layout/guest-layout.component";
import { UserLayoutComponent } from "./layouts/user-layout/user-layout.component";

const routes: Routes = [
  {
    component: GuestLayoutComponent,
    path: "",
    loadChildren: () => import("./modules/guest/guest.module").then((module) => module.GuestModule)
  },
  {
    component: UserLayoutComponent,
    path: "user",
    loadChildren: () => import("./modules/user/user.module").then((module) => module.UserModule)
  },
  {
    path: "**",
    redirectTo: "",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
