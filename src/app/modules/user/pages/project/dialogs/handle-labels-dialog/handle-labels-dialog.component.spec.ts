import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleLabelsDialogComponent } from './handle-labels-dialog.component';

describe('HandleLabelsDialogComponent', () => {
  let component: HandleLabelsDialogComponent;
  let fixture: ComponentFixture<HandleLabelsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleLabelsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleLabelsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
