import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleArchivesDialogComponent } from './handle-archives-dialog.component';

describe('HandleArchivesDialogComponent', () => {
  let component: HandleArchivesDialogComponent;
  let fixture: ComponentFixture<HandleArchivesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleArchivesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleArchivesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
