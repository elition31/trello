import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleInfoDialogComponent } from './handle-info-dialog.component';

describe('HandleInfoDialogComponent', () => {
  let component: HandleInfoDialogComponent;
  let fixture: ComponentFixture<HandleInfoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleInfoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
