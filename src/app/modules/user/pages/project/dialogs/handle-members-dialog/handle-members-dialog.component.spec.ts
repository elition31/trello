import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleMembersDialogComponent } from './handle-members-dialog.component';

describe('HandleMembersDialogComponent', () => {
  let component: HandleMembersDialogComponent;
  let fixture: ComponentFixture<HandleMembersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleMembersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleMembersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
