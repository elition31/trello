// Angular
import { Component, OnInit } from "@angular/core";

// Models
import { Project } from "src/app/core/models/project.interface";

@Component({
  selector: "app-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.scss"]
})
export class ProjectComponent implements OnInit {
  public project: Project;

  constructor() {}

  ngOnInit() {}
}
