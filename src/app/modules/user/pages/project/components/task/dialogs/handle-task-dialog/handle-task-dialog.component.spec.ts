import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleTaskDialogComponent } from './handle-task-dialog.component';

describe('HandleTaskDialogComponent', () => {
  let component: HandleTaskDialogComponent;
  let fixture: ComponentFixture<HandleTaskDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandleTaskDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleTaskDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
