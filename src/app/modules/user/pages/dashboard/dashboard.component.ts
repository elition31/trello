// Angular
import { Component } from "@angular/core";

// Angular Material
import { MatDialog } from "@angular/material/dialog";

// Services
import { ProjectService } from "src/app/core/services/project/project.service";
import { AuthService } from "src/app/core/services/auth/auth.service";

// Utils
import { timestampToDate } from "src/app/core/utils/date";

// Models
import { Project } from "src/app/core/models/project.interface";

// Dialogs Components
import { NewProjectDialogComponent } from "./dialogs/new-project-dialog/new-project-dialog.component";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent {
  public isLoading: boolean = true;

  // Stores all projects
  public personalProjects: Project[] = [];

  // Stores all projects
  public teamProjects: Project[] = [];

  // Stores the five most recent projects
  public recentProjects: Project[] = [];

  // Constructor
  constructor(private projectService: ProjectService, private dialog: MatDialog, private authService: AuthService) {
    // Page initialization
    this.initialize();
  }

  /**
   * Initialize all data
   */
  private async initialize(): Promise<void> {
    if (this.authService.auth$.value !== null) {
      // Get user ID
      const uid = this.authService.auth$.value.informations.uid;

      // Fetch Personal projects
      this.projectService.fetchPersonalProjects(uid).subscribe(async (projects: Project[]) => {
        this.personalProjects = projects;
        this.personalProjects = await this.sortProjectByDate(projects);
        this.recentProjects = this.personalProjects.slice(0, 5);
        this.isLoading = false;
      });

      // Fetch Team projects
      this.projectService.fetchTeamProjects(uid).subscribe(async (projects: Project[]) => {
        this.teamProjects = projects;
        this.teamProjects = await this.sortProjectByDate(projects);
      });
    }
  }

  /**
   * Sort projects from most recent to oldest
   * @param projects an array of projects
   */
  private async sortProjectByDate(projects: Project[]): Promise<Project[]> {
    projects = await this.cleanDates(projects);
    return projects.sort((a, b) => b.updatedAt - a.updatedAt);
  }

  /**
   * Dates from Firebase are in a specific format. We need to convert them in Javascript Date format
   * @param projects an array of projects
   */
  private cleanDates(projects: Project[]): Promise<Project[]> {
    return new Promise((resolve, reject) => {
      for (const project of projects) {
        project.updatedAt = timestampToDate(project.updatedAt.seconds);
        project.createdAt = timestampToDate(project.createdAt.seconds);
      }
      resolve(projects);
    });
  }

  /**
   * Open a dialog to allow user to create a new project
   */
  public createNewProject(): void {
    // Open Modal
    const newProjectDialog = this.dialog.open(NewProjectDialogComponent, { minWidth: "450px", data: this.authService.auth$.value.informations });

    // Observe the modal closure
    newProjectDialog.afterClosed().subscribe((result: Project) => {
      if (result !== undefined) {
        this.projectService.createNewProject(result);
      }
    });
  }
}
