// Angular
import { Component, Inject } from "@angular/core";
import { FormGroup, FormControl, Validators, AbstractControl } from "@angular/forms";

// Angular Material
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

// Models
import { Project } from "src/app/core/models/project.interface";
import { UserInfos } from "src/app/core/models/user-infos.interface";

@Component({
  selector: "app-new-project-dialog",
  templateUrl: "./new-project-dialog.component.html",
  styleUrls: ["./new-project-dialog.component.scss"]
})
export class NewProjectDialogComponent {
  public form: FormGroup;
  private color: string = "";

  constructor(public dialogRef: MatDialogRef<NewProjectDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: UserInfos) {
    this.form = new FormGroup({
      title: new FormControl("", [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      description: new FormControl("", [Validators.required, Validators.minLength(20)]),
      isPrivate: new FormControl(true, [Validators.required])
    });
  }

  get title(): AbstractControl {
    return this.form.get("title");
  }

  get description(): AbstractControl {
    return this.form.get("description");
  }

  get isPrivate(): AbstractControl {
    return this.form.get("isPrivate");
  }

  public selectColor(color: string): void {
    this.color = color;
  }

  public onSubmit(): void {
    // Initialize the project
    const project: Project = {
      id: "",
      title: this.title.value,
      description: this.description.value,
      owner: {
        id: this.data.uid,
        firstname: this.data.firstname,
        lastname: this.data.lastname
      },
      members: [],
      isPrivate: this.isPrivate.value,
      backgroundColor: this.color,
      boards: [],
      labels: [
        { text: "Label 1", backgroundColor: "#27ae60", fontColor: "#ffffff" },
        { text: "Label 2", backgroundColor: "#8e44ad", fontColor: "#ffffff" },
        { text: "Label 3", backgroundColor: "#2c3e50", fontColor: "#ffffff" },
        { text: "Label 4", backgroundColor: "#e67e22", fontColor: "#ffffff" },
        { text: "Label 5", backgroundColor: "#c0392b", fontColor: "#ffffff" },
        { text: "Label 6", backgroundColor: "#3498db", fontColor: "#ffffff" },
        { text: "Label 7", backgroundColor: "#16a085", fontColor: "#ffffff" }
      ],
      archives: [],
      createdAt: new Date(),
      updatedAt: new Date()
    };

    this.dialogRef.close(project);
  }

  public onClose(): void {
    this.dialogRef.close();
  }
}
