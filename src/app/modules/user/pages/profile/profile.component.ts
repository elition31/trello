// Angular
import { Component } from "@angular/core";
import { FormGroup, Validators, AbstractControl, FormControl } from "@angular/forms";
import { Router } from "@angular/router";

// Services
import { AuthService } from "src/app/core/services/auth/auth.service";
import { UserService } from "src/app/core/services/user/user.service";
import { ErrorsService } from "src/app/core/services/error/errors.service";
import { ToastService } from "src/app/core/services/toast/toast.service";

// Models
import { AuthInfos } from "src/app/core/models/auth-infos.interface";
import { UserInfos } from "src/app/core/models/user-infos.interface";

// Utils
import { timestampToDate, formatDate } from "src/app/core/utils/date";
@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent {
  // User data
  public user: AuthInfos;

  // Page Forms
  public formUserInformations: FormGroup;
  public formUserPassword: FormGroup;

  /**
   * Constructor
   * @param authService Handle Authentication
   */
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private toastService: ToastService,
    private errorsService: ErrorsService,
    private router: Router
  ) {
    this.authService.auth$.subscribe(async (auth) => {
      if (auth) {
        // Set user variable
        this.user = auth;

        this.formUserInformations = new FormGroup({
          id: new FormControl({ value: auth.user.uid, disabled: true }),
          email: new FormControl({ value: auth.user.email, disabled: true }),
          firstname: new FormControl({ value: auth.informations.firstname, disabled: false }, [Validators.required]),
          lastname: new FormControl({ value: auth.informations.lastname, disabled: false }, [Validators.required]),
          createdAt: new FormControl({ value: formatDate(timestampToDate(auth.informations.createdAt.seconds)), disabled: true }),
          updatedAt: new FormControl({ value: formatDate(timestampToDate(auth.informations.updatedAt.seconds)), disabled: true })
        });

        this.formUserPassword = new FormGroup({
          password: new FormControl("nofill", [
            Validators.minLength(6),
            Validators.required,
            Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{6,}")
          ]),
          confirmPassword: new FormControl("nofill", [
            Validators.minLength(6),
            Validators.required,
            Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{6,}")
          ])
        });

        // Prevent issue with Browser input's autofill
        setTimeout(() => {
          this.password.setValue("");
          this.confirmPassword.setValue("");
        }, 1200);
      } else {
        this.router.navigateByUrl("/signin");
      }
    });
  }

  get id(): AbstractControl {
    return this.formUserInformations.get("id");
  }

  get firstname(): AbstractControl {
    return this.formUserInformations.get("firstname");
  }

  get lastname(): AbstractControl {
    return this.formUserInformations.get("lastname");
  }

  get createdAt(): AbstractControl {
    return this.formUserInformations.get("createdAt");
  }

  get updatedAt(): AbstractControl {
    return this.formUserInformations.get("updatedAt");
  }

  get email(): AbstractControl {
    return this.formUserInformations.get("email");
  }

  get password(): AbstractControl {
    return this.formUserPassword.get("password");
  }

  get confirmPassword(): AbstractControl {
    return this.formUserPassword.get("confirmPassword");
  }

  public passwordDoestMatch(): boolean {
    return this.password.value === this.confirmPassword.value;
  }

  public async handleProfile(): Promise<void> {
    const data: Partial<UserInfos> = {
      uid: this.id.value,
      firstname: this.firstname.value,
      lastname: this.lastname.value,
      updatedAt: new Date()
    };

    try {
      // Update user's informations
      await this.userService.updateUser(data);

      // Update BehaviorSubject
      const { user, informations } = this.authService.auth$.value;
      this.authService.auth$.next({ user, informations: { ...informations, firstname: this.firstname.value, lastname: this.lastname.value } });

      // Display a message
      this.toastService.open("Votre profil a été modifié.");
    } catch (error) {
      this.errorsService.addError(error.message, "user");
    }
  }

  public async updatePassword(): Promise<void> {
    this.userService.updatePassword(this.password.value);
  }
}
