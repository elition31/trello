// Angular
import { NgModule } from "@angular/core";

// Routing Module
import { UserRoutingModule } from "./user-routing.module";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Pages
import { ProfileComponent } from "./pages/profile/profile.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { ProjectComponent } from "./pages/project/project.component";

// Components
import { ProjectNavbarComponent } from "./pages/project/components/project-navbar/project-navbar.component";
import { ListComponent } from "./pages/project/components/list/list.component";
import { TaskComponent } from "./pages/project/components/task/task.component";

// Dialogs
import { NewProjectDialogComponent } from "./pages/dashboard/dialogs/new-project-dialog/new-project-dialog.component";
import { RemoveProjectDialogComponent } from "./pages/project/dialogs/remove-project-dialog/remove-project-dialog.component";
import { HandleLabelsDialogComponent } from "./pages/project/dialogs/handle-labels-dialog/handle-labels-dialog.component";
import { HandleMembersDialogComponent } from "./pages/project/dialogs/handle-members-dialog/handle-members-dialog.component";
import { HandleArchivesDialogComponent } from "./pages/project/dialogs/handle-archives-dialog/handle-archives-dialog.component";
import { HandleInfoDialogComponent } from "./pages/project/dialogs/handle-info-dialog/handle-info-dialog.component";
import { HandleTaskDialogComponent } from "./pages/project/components/task/dialogs/handle-task-dialog/handle-task-dialog.component";

@NgModule({
  declarations: [
    ProfileComponent,
    DashboardComponent,
    NewProjectDialogComponent,
    ProjectComponent,
    RemoveProjectDialogComponent,
    HandleLabelsDialogComponent,
    HandleMembersDialogComponent,
    HandleArchivesDialogComponent,
    HandleInfoDialogComponent,
    ProjectNavbarComponent,
    ListComponent,
    TaskComponent,
    HandleTaskDialogComponent
  ],
  imports: [SharedModule, UserRoutingModule],
  entryComponents: [
    NewProjectDialogComponent,
    RemoveProjectDialogComponent,
    HandleLabelsDialogComponent,
    HandleMembersDialogComponent,
    HandleArchivesDialogComponent,
    HandleInfoDialogComponent,
    HandleTaskDialogComponent
  ]
})
export class UserModule {}
