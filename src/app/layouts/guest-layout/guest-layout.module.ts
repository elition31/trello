import { NgModule } from "@angular/core";

// Modules
import { SharedLayoutModule } from "../shared-layout/shared-layout.module";

// Components
import { GuestFooterComponent } from "./guest-footer/guest-footer.component";
import { GuestHeaderComponent } from "./guest-header/guest-header.component";
import { GuestLayoutComponent } from "./guest-layout.component";

@NgModule({
  declarations: [GuestFooterComponent, GuestHeaderComponent, GuestLayoutComponent],
  imports: [SharedLayoutModule],
  exports: [GuestLayoutComponent]
})
export class GuestLayoutModule {}
