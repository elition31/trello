/**
 * Convert a timestamp in seconds into Date
 */
export const timestampToDate = (timestamp: number): Date => {
  return new Date(timestamp * 1000);
};

/**
 * Convert and format a number into a string
 */
export const formatNumber = (value: number): string => {
  return value < 10 ? `0${value}` : `${value}`;
};

/**
 * Format a date to be read easily
 */
export const formatDate = (date: Date): string => {
  const year: number = date.getFullYear();
  const month: string = formatNumber(date.getMonth() + 1);
  const day: string = formatNumber(date.getDate());
  const hour: string = formatNumber(date.getHours());
  const minute: string = formatNumber(date.getMinutes());
  const second: string = formatNumber(date.getSeconds());

  return `Le ${day}/${month}/${year} à ${hour}:${minute}:${second}`;
};

