import { Injectable } from "@angular/core";

// Firebase
import { AngularFirestore } from "@angular/fire/firestore";

// Services
import { ErrorsService } from "../error/errors.service";
import { ToastService } from "../toast/toast.service";

// Models
import { Project } from "../../models/project.interface";
import { Observable, BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ProjectService {
  /**
   * Constructor
   * @param db Handle Firebase Database
   * @param errorsService Handle application's errors
   * @param toastService Handle toast notifications in the application
   */
  constructor(private db: AngularFirestore, private errorsService: ErrorsService, private toastService: ToastService) {}

  /**
   * Create a new personal project
   * @param project a project given
   */
  public async createNewProject(project: Project): Promise<void> {
    try {
      project.id = this.db.createId();

      await this.db
        .collection<Project>("projects")
        .doc<Project>(project.id)
        .set(project);
    } catch (error) {
      this.errorsService.addError(error.message, "project");
    }
  }

  /**
   * Fetch personal projects
   * @param id User ID
   */
  public fetchPersonalProjects(id: string): Observable<Project[]> {
    return this.db
      .collection<Project>("projects", (ref) => ref.where("owner.id", "==", id))
      .valueChanges();
  }

  /**
   * Fetch team projects
   * @param id User ID
   */
  public fetchTeamProjects(id: string): Observable<Project[]> {
    return this.db
      .collection<Project>("projects", (ref) => ref.where("members", "array-contains", id))
      .valueChanges();
  }

  /**
   * Fetch a project by its ID
   * @param id project ID
   */
  public fetchProjectById(id: string): Observable<Project> {
    return this.db
      .collection<Project>("projects").doc<Project>(id).valueChanges();
  }
}
