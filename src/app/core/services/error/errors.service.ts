import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { ServiceErrors } from "../../models/service-errors.interfae";

@Injectable({
  providedIn: "root"
})
export class ErrorsService {
  public errors$: BehaviorSubject<ServiceErrors> = new BehaviorSubject<ServiceErrors>(null);

  constructor() {
    this.errors$.next({ auth: null, user: null, toast: null, project: null });
  }

  public addError(value: string, service: string) {
    const errors: ServiceErrors = this.errors$.value;
    errors[service] = value;
    this.errors$.next(errors);
  }

  public deleteError(service: string) {
    const errors: ServiceErrors = this.errors$.value;
    errors[service] = null;
    this.errors$.next(errors);
  }
}
