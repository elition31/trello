export interface Activity {
  date: any;
  owner: {
    id: string;
    firstname: string;
    lastname: string;
  };
  value: string;
}
