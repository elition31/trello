export interface Board {
  id: string;
  order: number;
  title: string;
  tasks: [];
}