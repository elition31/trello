export interface Comment {
  id: string;
  owner: {
    id: string;
    firstname: string;
    lastname: string;
  };
  value: string;
  createdAt: any;
  updatedAt: any;
}
