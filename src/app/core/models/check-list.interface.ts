export interface CheckList {
  order: number;
  value: string;
  isChecked: boolean;
  checkList?: CheckList[];
}
