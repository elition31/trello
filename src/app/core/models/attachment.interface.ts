export interface Attachment {
  name: string;
  extension: string;
  reference: string;
}