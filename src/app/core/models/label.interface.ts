export interface Label {
  text: string;
  backgroundColor: string;
  fontColor: string;
}