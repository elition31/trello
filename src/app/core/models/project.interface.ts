import { Board } from "./board.interface";
import { Label } from "./label.interface";
import { Task } from "./task-interface";

export interface Project {
  id: string;
  title: string;
  description: string;
  owner: {
    id: string;
    firstname: string;
    lastname: string;
  };
  backgroundColor: string;
  members: string[];
  isPrivate: boolean;
  boards: Board[];
  labels: Label[];
  archives: Task[];
  createdAt: any;
  updatedAt: any;
}
