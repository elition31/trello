import { User } from "firebase";
import { UserInfos } from "./user-infos.interface";

export interface AuthInfos {
  user?: User;
  informations?: UserInfos;
}
