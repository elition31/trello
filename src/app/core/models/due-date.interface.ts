export interface DueDate {
  title: string;
  description: string;
  isDone: boolean;
  date: {
    year: number;
    month: number;
    day: number;
  };
  time: {
    hour: number;
    minute: number;
  };
}
