import { User } from "firebase";

export interface UserInfos {
  uid?: string;
  firstname?: string;
  lastname?: string;
  createdAt?: any;
  updatedAt?: any;
}
