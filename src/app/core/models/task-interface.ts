import { Label } from "./label.interface";
import { DueDate } from "./due-date.interface";
import { Activity } from "./activity.interface";
import { Attachment } from "./attachment.interface";
import { CheckList } from "./check-list.interface";
import { Comment } from "./comment.interface";

export interface Task {
  id: string;
  priority: number;
  labels: Label[];
  title: string;
  description: string;
  isDone: boolean;
  dueDates: DueDate[];
  people: string[];
  comments: Comment[];
  activities: Activity[];
  attachments: Attachment[];
  checkList: CheckList[];
  createdAt: any;
  updatedAt: any;
}
