import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-color-picker",
  templateUrl: "./color-picker.component.html",
  styleUrls: ["./color-picker.component.scss"]
})
export class ColorPickerComponent implements OnInit {
  @Output() updated = new EventEmitter<string>();
  public selected: number = 0;
  public colors: string[] = ["#2c3e50", "#95a5a6", "#e67e22", "#f1c40f", "#2980b9", "#9b59b6", "#2ecc71", "#e84393", "#e17055", "#6c5ce7"];
 
  constructor() {}

  ngOnInit(): void {
    this.updated.emit(this.colors[0]);
  }

  public choose(index: number): void {
    this.updated.emit(this.colors[index]);
    this.selected = index;
  }
}
