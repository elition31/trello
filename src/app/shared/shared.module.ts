// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

// Angular Material Module
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatDialogModule } from "@angular/material/dialog";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatIconModule } from "@angular/material/icon";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatListModule } from "@angular/material/list";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule } from "@angular/material/sidenav";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";

// Components
import { ColorPickerComponent } from "./components/color-picker/color-picker.component";

// Custom Components
const COMPONENTS = [ColorPickerComponent];

// Material Modules
const MATERIAL_MODULES = [
  MatInputModule,
  MatButtonModule,
  MatSnackBarModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule,
  MatListModule,
  MatToolbarModule,
  MatSidenavModule,
  DragDropModule,
  MatSlideToggleModule
];

// All Modules
const MODULES = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, ...MATERIAL_MODULES];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...MODULES, ...COMPONENTS]
})
export class SharedModule {}
